import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storage_info/storage_info.dart';

import 'color.dart';
import 'list.dart';
import 'package:battery/battery.dart';
import 'package:geolocator/geolocator.dart';


class profilePage extends StatefulWidget {
  @override
  profilePageState createState() => profilePageState();

}

class profilePageState extends State<profilePage> {
  Future<double> _getSpace() async {
    return await StorageInfo.getStorageFreeSpaceInGB;
  }
  Future<double> _getSspace() async {
    return await StorageInfo.getExternalStorageUsedSpaceInGB;
  }
  final Battery _battery = Battery();
  BatteryState? _batteryState;
  late StreamSubscription<BatteryState> _batteryStateSubscription;

  @override
  void initState() {
    super.initState();
    _batteryStateSubscription =
        _battery.onBatteryStateChanged.listen((BatteryState state) {
          setState(() {
            _batteryState = state;
          });
        });
  }
  var h;
  var g;
void location() async{
    LocationPermission permission=await Geolocator.checkPermission();
    if(permission==LocationPermission.denied||permission==LocationPermission.deniedForever){
      print("Not found");
      LocationPermission ppremission=await Geolocator.requestPermission();
    }else{
      Position currentpostion=await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
       h=currentpostion.latitude;
       g=currentpostion.longitude;
      print(currentpostion.latitude);
      print(currentpostion.longitude);
    }
}
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'My Profile',
          ),
          centerTitle: true,
          backgroundColor: Colors.grey[700]!.withOpacity(0.4),
          elevation: 0,
          // give the app bar rounded corners
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
          leading: Icon(
            Icons.menu,
          ),
        ),
        body: Column(
          children: <Widget>[
            // construct the profile details widget here
            SizedBox(
              height: 180,
              child: Center(
                child: Text(
                  'Profile Details Goes here',
                ),
              ),
            ),

            // the tab bar with two items
            SizedBox(
              height: 50,
              child: AppBar(
                bottom: TabBar(
                  tabs: [
                    Tab(
                      icon: Icon(Icons.add_shopping_cart_outlined),
                    ),
                    Tab(
                      icon: Icon(
                        Icons.auto_awesome_mosaic_rounded,
                      ),
                    ),
                    Tab(
                      icon: Icon(
                        Icons.autorenew_sharp,
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // create widgets for each tab bar here
            Expanded(
              child: TabBarView(
                children: [
                  MyHomePage(),
                  // first tab bar view widget
                  Column(
                    crossAxisAlignment:CrossAxisAlignment.start,
                    mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                        children: [
                          Text("Batrry Status",style:TextStyle(
                              color:Colors.black
                          ),),
                          ElevatedButton(
                            child: const Icon(Icons.battery_unknown),
                            onPressed: () async {
                              final int batteryLevel = await _battery.batteryLevel;
                              // ignore: unawaited_futures
                              showDialog<void>(
                                context: context,
                                builder: (_) => AlertDialog(
                                  content: Text('Battery: $batteryLevel%'),
                                  actions: <Widget>[
                                    TextButton(
                                      child: const Text('OK'),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    )
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                        children: [
                          Text("Space Status",style:TextStyle(
                              color:Colors.black
                          ),),
                          Column(
                            children: [
                              _Storage(),
                              _Sstorage(),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                        children: [
                          RaisedButton(
                            child:Text("location"),
                            onPressed: (){
                              location();
                            },
                          ),
                          Column(
                            children: [
                              Text(h.toString()),
                              Text(g.toString()),
                            ],
                          ),


                        ],
                      ),
                      // Container(
                      //   color:  ColorUtils.primaryColor,
                      //   child: Center(
                      //     child: Text(
                      //       'Dashbord',
                      //     ),
                      //   ),
                      // ),


                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Container(
                        height: 100,
                        width: 100,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary:  ColorUtils.accentColor,
                            onPrimary: Colors.white,
                            shadowColor: ColorUtils.accentColor,
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32.0)),
                            minimumSize: Size(100, 40), //////// HERE
                          ),
                          onPressed: () {},
                          child: Text('Save'),
                        ),
                      ),
                    ],
                  )


                  // second tab bar viiew widget
                  // Container(
                  //   color: Colors.pink,
                  //   child: Center(
                  //     child: Text(
                  //       'product',
                  //     ),
                  //   ),
                  // ),

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget _Storage() {
    return FutureBuilder(
        future: _getSpace(),
        builder: (context, snapshot) {
          print(snapshot.error);
          if (snapshot.hasData) {
            return Text('Space: ${snapshot.data}');
          } else {
            return Text("Loading");
          }
        });
  }
  Widget _Sstorage() {
    return FutureBuilder(
        future: _getSspace(),
        builder: (context, snapshot) {
          print(snapshot.error);
          if (snapshot.hasData) {
            return Text('Space: ${snapshot.data}');
          } else {
            return Text("Loading");
          }
        });
  }

}