// import 'package:flutter/material.dart';
//
//
// class HomePage extends StatefulWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }
//
// class _HomePageState extends State<HomePage> {
//
//   Widget _getListItemTile(BuildContext context, int index) {
//     return Card(
//       child: GestureDetector(
//         child: ListTile(
//           leading: Icon(Icons.check_box_outline_blank),
//           title: Text('My Item'),
//           onTap: () {},
//         ),
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: GestureDetector(
//         onTap: (){
//
//         },
//         child: ListView.builder(
//           itemCount: 7,
//           itemBuilder: _getListItemTile,
//         ),
//       ),
//     );
//   }
// }
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:task/product.dart';
import 'package:task/productdeatail.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  static List<String> fruitname =['Apple','Banana','Mango','Orange','pineapple'];
  static List<String> Price =['10pkr','10pkr','10pkr','10pkr','10pkr'];
  static List<String> Description =['acha','acha','acha','acha','acha'];
  static List url = ['https://www.applesfromny.com/wp-content/uploads/2020/05/Jonagold_NYAS-Apples2.png',
    'https://cdn.mos.cms.futurecdn.net/42E9as7NaTaAi4A6JcuFwG-1200-80.jpg',
    'https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Hapus_Mango.jpg/220px-Hapus_Mango.jpg',
    'https://5.imimg.com/data5/VN/YP/MY-33296037/orange-600x600-500x500.jpg',
    'https://5.imimg.com/data5/GJ/MD/MY-35442270/fresh-pineapple-500x500.jpg'];
  final List<FruitDataModel> Fruitdata = List.generate(
      fruitname.length,
          (index)
      => FruitDataModel('${fruitname[index]}', '${url[index]}','${fruitname[index]}','${Description[index]}', '${Price[index]}'));
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
            itemCount: Fruitdata.length,
            itemBuilder: (context,index){
              return Card(
                child: ListTile(
                  title: Text(Fruitdata[index].name),
                  leading: SizedBox(
                    width: 50,
                    height: 50,
                    child: Image.network(Fruitdata[index].ImageUrl),
                  ),
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FruitDetail(fruitDataModel: Fruitdata[index],)));
                  },
                ),
              );
            }
        )
    );
  }
}